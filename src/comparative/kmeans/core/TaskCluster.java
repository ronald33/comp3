package comparative.kmeans.core;

import java.util.Arrays;
import java.util.List;

public class TaskCluster {

	private List<TaskItem> listTaskItem;
	private TaskItem centerTaskItem;

	public TaskCluster(List<TaskItem> listTaskItem, TaskItem centerTaskItem) {
		super();
		this.listTaskItem = listTaskItem;
		this.centerTaskItem = centerTaskItem;
	}

	public void calculateMean(){
		
		int stageSize = centerTaskItem.getArrayWeights().length;
		
		double arrayWeightsSum[] = new double[stageSize];
		double arrayWeightsMean[] = new double[stageSize];
		
		Arrays.fill(arrayWeightsSum, 0.0f);
		
		for(TaskItem taskItem:listTaskItem){
			
			double arrayTempWeights[] = taskItem.getArrayWeights();
			
			for(int i=0; i < stageSize; i++ ){		
				
				arrayWeightsSum[i] = arrayWeightsSum[i] + arrayTempWeights[i];
			}
		}
		
		for(int i = 0; i< stageSize; i++){
			arrayWeightsMean[i]  = arrayWeightsSum[i] / listTaskItem.size();
		}
		
		centerTaskItem.setArrayWeights(arrayWeightsMean);	
		
	}

	public List<TaskItem> getListTaskItem() {
		return listTaskItem;
	}

	public void setListTaskItem(List<TaskItem> listTaskItem) {
		this.listTaskItem = listTaskItem;
	}

	public TaskItem getCenterTaskItem() {
		return centerTaskItem;
	}

	public void setCenterTaskItem(TaskItem centerTaskItem) {
		this.centerTaskItem = centerTaskItem;
	}
	
}