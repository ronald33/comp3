package comparative.dbscan_kdtree.core;

import java.util.ArrayList;
import java.util.List;

public class Cluster
{
	private List<Point> points = new ArrayList<>();
	
	public void addPoint(Point p)
	{
		p.hasCluster();
		points.add(p);
	}
	
	public List<Point> getPoints()
	{
		return this.points;
	}
}
