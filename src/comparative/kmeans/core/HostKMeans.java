package comparative.kmeans.core;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class HostKMeans {

	private List<TaskCluster> listMapTaskCluster;
	private List<TaskCluster> listReduceTaskCluster;
	private List<TaskItem> listMapTaskItem;
	private List<TaskItem> listReduceTaskItem;
	
	public final int NUM_MAP_CLUSTERS = 20;
	public final int NUM_REDUCE_CLUSTERS = 20;
	private final int ITERATIONS_NUMBER = 3;
	
	public enum TaskType {MAP, REDUCE};
	
	private TaskType taskType;
	public String JobID; // this temp
	
	public HostKMeans(List<TaskCluster> listMapTaskCluster, List<TaskCluster> listReduceTaskCluster,
			List<TaskItem> listMapTaskItem, List<TaskItem> listReduceTaskItem, String JobID) {
		super();
		this.listMapTaskCluster = listMapTaskCluster;
		this.listReduceTaskCluster = listReduceTaskCluster;
		this.listMapTaskItem = listMapTaskItem;
		this.listReduceTaskItem = listReduceTaskItem;
		this.JobID = JobID;
	}

	public void run(){	
		
		// System.out.println(">>>> Initializing ...");
		
		// System.out.println(">>>> For Map");
		taskType = TaskType.MAP;
		initCenters(listMapTaskItem, listMapTaskCluster, taskType);
		
		// System.out.println(">>>> For Reduce");
		taskType = TaskType.REDUCE;
		initCenters(listReduceTaskItem, listReduceTaskCluster, taskType);
		
		int iter = 0;
		int stageSize;
		
		while(iter < ITERATIONS_NUMBER){
			
			if(iter != 0){

				cleanAllListTaskCluster();
			}
			
			// System.out.println(" >>>> Iter: "+iter);  
			// System.out.println(" >>>> For Map");  
			
			stageSize = listMapTaskCluster.get(0).getCenterTaskItem().getArrayWeights().length;
			calculateBestDistance(listMapTaskItem, listMapTaskCluster, stageSize);
	
			// System.out.println(" >>>> Print for Map Task Cluster"); 
			printListAll(listMapTaskCluster);
			
			// System.out.println(" >>>> For Reduce");  
			stageSize = listReduceTaskCluster.get(0).getCenterTaskItem().getArrayWeights().length;
			calculateBestDistance(listReduceTaskItem, listReduceTaskCluster, stageSize);	
			
			// System.out.println(" >>>> Print for Reduce Task Cluster");  
			printListAll(listReduceTaskCluster);		
			
			
			calculateMeanListTaskItem(listMapTaskCluster);
			calculateMeanListTaskItem(listReduceTaskCluster);
			
			
			iter++;
		}

		writeHistoricalInformationCluster();

	}
	
	public void addTaskCluster(){
		
	}
		
	public void initCenters(List<TaskItem> listTaskItem, List<TaskCluster> listTaskCluster, TaskType taskType){
		
		//init listTaskCluster
		int numClusters = 0;
		int size = listTaskItem.size();
		
		if(taskType == TaskType.MAP){
			if(size >= NUM_MAP_CLUSTERS){
				numClusters = NUM_MAP_CLUSTERS;
			}else{
				numClusters = 1;
			}
		} else{
			if(size >= NUM_REDUCE_CLUSTERS){
				numClusters = NUM_REDUCE_CLUSTERS;				
			}else{
				numClusters = 1;				
			}
		}

		for (int i = 0; i < numClusters ; i++) {
			listTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
		}
		
		// init centers		
		Random random = new Random();	
		
		// System.out.println(" >>>> Print size listTaskItem: " + size);  
	
		int randIndex = -1;
		int arrayRandIndex[] = new int[listTaskCluster.size()];
		int index = 0;
		
		Arrays.fill(arrayRandIndex, -1);
		
		for(TaskCluster taskCluster: listTaskCluster){
			
			randIndex = (int) (random.nextDouble()*size);
			// System.out.println(" >>>> Print random index for listTaskItem: " + randIndex);  
			arrayRandIndex[index] = randIndex;
			
			boolean isRandom = false;
			
			while(true){
				// System.out.println(">>>> Loop ...");
				
				for (int j = 0; j < arrayRandIndex.length; j++){
					if(randIndex == arrayRandIndex[j] &&  index != j ){
						randIndex = (int) (random.nextDouble()*size);
						// System.out.println(" >>>> Print random index for listTaskItem (inner): " + randIndex); 
						arrayRandIndex[index] = randIndex;
						isRandom = false;
						break;
					}else{
						isRandom = true;
					}
				}
				if(isRandom == true){
					break;
				}
			}
			
			taskCluster.setCenterTaskItem(new TaskItem(listTaskItem.get(randIndex).getArrayWeights(), listTaskItem.get(randIndex).getTaskAttemptID()));
			
			index++;
		}
		
	}
	
	public void calculateBestDistance(List<TaskItem>  listTaskItem, List<TaskCluster> listTaskCluster, int size){
		
		double arrayTempWeights [] = new double[size];
		double weightsSum  = 0.0f;
		double tempDistance  = 0.0f;
		double distanceMin = 10000000000000.0f;
		//TaskItem taskClusterTemp = null;
		
		for(TaskItem taskItem:listTaskItem){
			
			int index = 0;
			int indexTemp = -1;
		
			for(TaskCluster taskCluster:listTaskCluster){
		
				for(int i = 0; i < size; i++){
					arrayTempWeights[i] = (double)Math.pow(taskItem.getArrayWeights()[i] - taskCluster.getCenterTaskItem().getArrayWeights()[i], 2); 
					weightsSum = weightsSum + arrayTempWeights[i];
				}
									
				tempDistance = (double)Math.pow(weightsSum, 2);

				if(distanceMin > tempDistance){
					distanceMin  = tempDistance;
					//taskClusterTemp = taskCluster.getCenterTaskItem();
					indexTemp = index;
				}
				
				index++;
				weightsSum  = 0.0f;
			}
			
			String weights = " ";
			String separator = ", ";
			
			for (int i = 0; i < arrayTempWeights.length; i++) {
				if(i == arrayTempWeights.length -1 ){
					separator = "";
				}
				weights = weights + taskItem.getArrayWeights()[i] + separator;
			}
			
//			// System.out.println(" >>>> Value: " + weights); 
			
			listTaskCluster.get(indexTemp).getListTaskItem().add(taskItem);      
			
			distanceMin = 10000000000000.0f;
		}
		

	}
	
	public void calculateMeanListTaskItem(List<TaskCluster> listTaskCluster){
		
		for(TaskCluster taskCluster: listTaskCluster){
			taskCluster.calculateMean();
		}
		
	} 
	
	public void cleanAllListTaskCluster(){
				
		for(TaskCluster taskCluster: listMapTaskCluster){
			taskCluster.getListTaskItem().clear();
		}
		
		for(TaskCluster taskCluster: listReduceTaskCluster){
			taskCluster.getListTaskItem().clear();
		}
			
	}

	public static void printListAll(List<TaskCluster> listTaskCluster){
		
		for (int i = 0; i < listTaskCluster.size(); i++) {
			
			// System.out.println(">>>> Print for Task Cluster " + i );
			TaskCluster taskCluster =  listTaskCluster.get(i);
			
			String weights = " ";
			String separator = ", ";
			
			for (int j = 0; j < taskCluster.getCenterTaskItem().getArrayWeights().length; j++) {
				if(j == taskCluster.getCenterTaskItem().getArrayWeights().length -1 ){
					separator = "";
				}
				weights = weights + taskCluster.getCenterTaskItem().getArrayWeights()[j] + separator;
			}
			// System.out.println(">>>> Print CenterTaskItem with value: " + weights); 

			
			// System.out.println(">>>> Print values for Task Cluster");
			for (int j = 0; j < taskCluster.getListTaskItem().size(); j++) {
				
				
				String weights2 = " ";
				String separator2 = ", ";				
				
				for (int k = 0; k < taskCluster.getListTaskItem().get(j).getArrayWeights().length; k++) {
					if(k == taskCluster.getListTaskItem().get(j).getArrayWeights().length -1 ){
						separator = "";
					}
					weights2 = weights2 + taskCluster.getListTaskItem().get(j).getArrayWeights()[k] + separator2;
				}				
//				// System.out.println(">>>> Value: " + weights2);

			}	
		}
	}
	
	public void writeHistoricalInformationCluster(){

		BufferedWriter outputMap = null;
		BufferedWriter outputReduce = null;
		
		// The structure is: <weight_1> ..  <weight_N> 
		
		try {
		
			File fileMap = new File(ESAMR.path + "clusterMapWeights-" + JobID + ".txt");
			outputMap = new BufferedWriter(new FileWriter(fileMap));

			for (int i = 0; i < listMapTaskCluster.size(); i++) {
				double mapWeights[] = listMapTaskCluster.get(i).getCenterTaskItem().getArrayWeights();
				outputMap.write(mapWeights[0] + " " + mapWeights[1]);
				outputMap.newLine();
			}

			File fileReduce =  new File(ESAMR.path + "clusterReduceWeights-" + JobID + ".txt");
			outputReduce = new BufferedWriter(new FileWriter(fileReduce));

			for (int i = 0; i < listReduceTaskCluster.size(); i++) {
				double reduceWeights[] = listReduceTaskCluster.get(i).getCenterTaskItem().getArrayWeights();
				outputReduce.write(reduceWeights[0]+ " " + reduceWeights[1]+ " " + reduceWeights[2]);
				outputReduce.newLine();
			}
		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {	
			try{
				if ( outputMap != null ) outputMap.close();
				if ( outputReduce != null ) outputReduce.close();	

			}catch ( IOException e ) {
				e.printStackTrace();
			}
		}
	}
	
	// Get and set
	
	public List<TaskCluster> getListMapTaskCluster() {
		return listMapTaskCluster;
	}

	public void setListMapTaskCluster(List<TaskCluster> listMapTaskCluster) {
		this.listMapTaskCluster = listMapTaskCluster;
	}

	public List<TaskCluster> getListReduceTaskCluster() {
		return listReduceTaskCluster;
	}

	public void setListReduceTaskCluster(List<TaskCluster> listReduceTaskCluster) {
		this.listReduceTaskCluster = listReduceTaskCluster;
	}

	public List<TaskItem> getListMapTaskItem() {
		return listMapTaskItem;
	}

	public void setListMapTaskItem(List<TaskItem> listMapTaskItem) {
		this.listMapTaskItem = listMapTaskItem;
	}

	public List<TaskItem> getListReduceTaskItem() {
		return listReduceTaskItem;
	}

	public void setListReduceTaskItem(List<TaskItem> listReduceTaskItem) {
		this.listReduceTaskItem = listReduceTaskItem;
	}


	
	
}