package comparative.test;

import java.util.ArrayList;
import java.util.List;

import comparative.dbscan_kdtree.core.Dbscan;
import comparative.dbscan_kdtree.core.Point;
import comparative.test.Comparation.AlgorithmType;

public class Test {

	public static void main(String[] args) throws Exception
	{
		
		/*
		List<TaskCluster> listMapTaskCluster = new ArrayList<TaskCluster>();
		List<TaskCluster> listReduceTaskCluster = new ArrayList<TaskCluster>();

		listMapTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
		listMapTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));

		listReduceTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
		listReduceTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));

		List<TaskItem> listMapTaskItem = new ArrayList<TaskItem>();
		listMapTaskItem.add(new TaskItem(new float[]{0.35f, 0.44f, 0.21f}, "1"));
		listMapTaskItem.add(new TaskItem(new float[]{0.30f, 0.40f, 0.30f}, "2"));
		listMapTaskItem.add(new TaskItem(new float[]{0.32f, 0.42f, 0.26f}, "3"));


		listMapTaskItem.add(new TaskItem(new float[]{0.15f, 0.34f, 0.51f}, "4"));
		listMapTaskItem.add(new TaskItem(new float[]{0.20f, 0.30f, 0.50f}, "5"));
		listMapTaskItem.add(new TaskItem(new float[]{0.22f, 0.32f, 0.46f}, "6"));


		List<TaskItem> listReduceTaskItem = new ArrayList<TaskItem>();
		listReduceTaskItem.add(new TaskItem(new float[]{0.35f, 0.44f, 0.21f}, "7"));
		listReduceTaskItem.add(new TaskItem(new float[]{0.30f, 0.40f, 0.30f}, "8"));
		listReduceTaskItem.add(new TaskItem(new float[]{0.32f, 0.42f, 0.26f}, "9"));


		listReduceTaskItem.add(new TaskItem(new float[]{0.15f, 0.34f, 0.51f}, "10"));
		listReduceTaskItem.add(new TaskItem(new float[]{0.20f, 0.30f, 0.50f}, "11"));
		listReduceTaskItem.add(new TaskItem(new float[]{0.22f, 0.32f, 0.46f}, "12"));*/

//		Comparation comp = new Comparation();
//		comp.
		
//		double eps = Math.sqrt(2);
//		int minPts = 2;
//		Dbscan dbscan = new Dbscan(eps, minPts);
//		Point p1 = new Point(new double[]{4f, 4f});
//		Point p2 = new Point(new double[]{2f, 4f});
//		Point p3 = new Point(new double[]{3f, 4f});
//		Point p4 = new Point(new double[]{4f, 2f});
//		Point p5 = new Point(new double[]{5f, 4f});
//		List<Point> points = new ArrayList<>();
//		points.add(p1);
//		points.add(p2);
//		points.add(p3);
//		points.add(p4);
//		points.add(p5);
//		dbscan.setPoints(points);
//		dbscan.initKdtree(2);
//		dbscan.setClusters();
//		dbscan.showClusters();
		
		double eps = Math.sqrt(8);
		int minPts = 2;
		Dbscan dbscan = new Dbscan(eps, minPts);

		List<Point> listPoint = new ArrayList<Point>();
//		listPoint.add(new Point(new double[]{4.0, 4.0}));
//		listPoint.add(new Point(new double[]{3.0, 4.0}));
//		listPoint.add(new Point(new double[]{2.0, 4.0}));
//		listPoint.add(new Point(new double[]{4.0, 4.0}));
//		listPoint.add(new Point(new double[]{9.0, 1.0}));
//		listPoint.add(new Point(new double[]{3.0, 1.0}));
//		listPoint.add(new Point(new double[]{2.0, 2.0}));
//		listPoint.add(new Point(new double[]{4.0, 5.0}));
//		listPoint.add(new Point(new double[]{5.0, 6.0}));
//		listPoint.add(new Point(new double[]{7.0, 1.0}));
//		listPoint.add(new Point(new double[]{5.0, 4.0}));
//		listPoint.add(new Point(new double[]{5.0, 20.0}));
//		listPoint.add(new Point(new double[]{25.0, 20.0}));
//		listPoint.add(new Point(new double[]{25.0, 21.0}));
//		listPoint.add(new Point(new double[]{25.0, 21.0}));
		
//		listPoint.add(new Point(new double[]{1.0, 2.0}));
//		listPoint.add(new Point(new double[]{3.0, 4.0}));
//		listPoint.add(new Point(new double[]{5.0, 6.0}));
//		listPoint.add(new Point(new double[]{11.0, 12.0}));
//		listPoint.add(new Point(new double[]{13.0, 14.0}));
//		listPoint.add(new Point(new double[]{15.0, 16.0}));
		
		
//		dbscan.setPoints(listPoint);
//		dbscan.initKdtree(2);
//		dbscan.setClusters();
//		System.out.println(dbscan.getSizeClusters());
//		System.exit(0);
		
		/*List<TaskCluster> listMapTaskCluster = new ArrayList<TaskCluster>();
		List<TaskCluster> listReduceTaskCluster = new ArrayList<TaskCluster>();

		listMapTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
		listMapTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));

		listReduceTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
		listReduceTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));

		List<TaskItem> listMapTaskItem = new ArrayList<TaskItem>();
		listMapTaskItem.add(new TaskItem(new double[]{0.35, 0.44, 0.21}));
		listMapTaskItem.add(new TaskItem(new double[]{0.30, 0.40, 0.30}));
		listMapTaskItem.add(new TaskItem(new double[]{0.32, 0.42, 0.26}));
		listMapTaskItem.add(new TaskItem(new double[]{0.15, 0.34, 0.51}));
		listMapTaskItem.add(new TaskItem(new double[]{0.20, 0.30, 0.50}));
		listMapTaskItem.add(new TaskItem(new double[]{0.22, 0.32, 0.46}));

		List<TaskItem> listReduceTaskItem = new ArrayList<TaskItem>();
		listReduceTaskItem.add(new TaskItem(new double[]{0.35, 0.44, 0.21}));
		listReduceTaskItem.add(new TaskItem(new double[]{0.30, 0.40, 0.30}));
		listReduceTaskItem.add(new TaskItem(new double[]{0.32, 0.42, 0.26}));
		listReduceTaskItem.add(new TaskItem(new double[]{0.15, 0.34, 0.51}));
		listReduceTaskItem.add(new TaskItem(new double[]{0.20, 0.30, 0.50}));
		listReduceTaskItem.add(new TaskItem(new double[]{0.22, 0.32, 0.46}));

		HostKMeans kmeans = new HostKMeans(listMapTaskCluster, listReduceTaskCluster, listMapTaskItem, listReduceTaskItem, "Job1");
		kmeans.run();*/
		
		Comparation c = new Comparation();
		
		c.evaluateAlgorithm(AlgorithmType.DBSCAN_KDTREE, 2048);
		c.evaluateAlgorithm(AlgorithmType.KMEANS, 2048);
		
		System.out.println("Fin ...");
	}

}
