package comparative.kmeans.core;


public class TaskItem {
	
	private double arrayWeights[];
	private String taskAttemptID;
	
	
	public TaskItem(double[] arrayWeights, String taskAttemptID) {
		super();
		this.arrayWeights = arrayWeights;
		this.taskAttemptID = taskAttemptID;
	}
	
	public TaskItem(double[] arrayWeights) {
		super();
		this.arrayWeights = arrayWeights;
	}
	
	public double[] getArrayWeights() {
		return arrayWeights;
	}
	public void setArrayWeights(double[] arrayWeights) {
		this.arrayWeights = arrayWeights;
	}

	public String getTaskAttemptID() {
		return taskAttemptID;
	}

	public void setTaskAttemptID(String taskAttemptID) {
		this.taskAttemptID = taskAttemptID;
	}	
}
