package comparative.generator;

public class Measure {
	public long startTime;
    public long endTime;
    
    public Measure() {
		this.start();
	}

    public long getDuration() {
    	this.stop();
//        return (endTime - startTime) / 1000;
        return (endTime - startTime);
    }
    
    public void showDuration(String phase) {
		System.out.println("En " + phase + " se tardo: " + this.getDuration() + " segundos");
	}
    // I  would add
    public void start() {
        startTime = System.currentTimeMillis();
    }
    public void stop() {
         endTime = System.currentTimeMillis();
     }
}
