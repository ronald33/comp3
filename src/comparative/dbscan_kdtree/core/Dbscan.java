package comparative.dbscan_kdtree.core;
import java.util.ArrayList;
import java.util.List;

import comparative.generator.Measure;

public class Dbscan
{
	private double eps;
	private double minPts;
//	private int cluster;
	private KdTree kdtree;
	
	private List<Cluster> clusters = new ArrayList<>();
	
	private List<Point> points = new ArrayList<Point>();
	
	public Dbscan(double eps, double minPts) {
		super();
		this.eps = eps;
		this.minPts = minPts;
//		this. cluster = -1;
	}
	
	public void initKdtree(int dim){
		Measure m = new Measure();
		kdtree = new KdTree(dim, points, eps);
		kdtree.buildTree();
		m.showDuration("construir el arbol");
		
	}
	
	public void setClusters()
	{
        for (Point point : this.points)
        {
        	if(point.isVisited()) { continue; } 
        	else
        	{
        		point.visited();
        		List<Point> neighbours = regionQueryKDTree(point);
//        		List<Point> neighbours = regionQuery(point);
            	if(neighbours.size() < this.minPts) { point.noise(); }
            	else
            	{
//            		this.cluster++;
            		Cluster c = new Cluster();
            		this.clusters.add(c);
            		this.expandCluster(point, neighbours, c); 
            	}
        	}
        }
	}
	
	private void expandCluster(Point p, List<Point> points, Cluster c)
	{
//		p.setCluster(this.cluster);
		c.addPoint(p);
		for (int i = 0; i < points.size(); i++)
		{
			Point point = points.get(i);
			if(!point.isVisited())
			{
				point.visited();
				List<Point> neighbours =  regionQueryKDTree(point);
//				List<Point> neighbours =  regionQuery(point);
				
				if(neighbours.size() >= this.minPts)
	        	{
					for (Point n : neighbours)
					{
						if(!points.contains(n)) { points.add(n); }
					}
	        	}
			}
			if(!point.hasCluster()) { /*point.setCluster(this.cluster);*/ c.addPoint(point); }
		}
	}

	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}
	
	@SuppressWarnings("unused")
	private List<Point> regionQuery(Point center)
	{
		List<Point> neighbours = new ArrayList<>();
		for (Point point : this.points)
        {
			Double distance = center.distance(point);
			if(distance <= eps) { neighbours.add(point); }
        }
		return neighbours;
	}
	
	private List<Point> regionQueryKDTree(Point center){
		
		List<Point> listNearest = kdtree.searchNearestNeighborsPoints(center);
		return listNearest;
		
	}
	
	public void showClusters()
	{
		int size = this.clusters.size();
		
		for (int i = 0; i < size; i++)
		{
			System.out.println(i);
			System.out.println(this.clusters.get(i).getPoints().toString());
		}
	}
	
	public int getSizeClusters() { return this.clusters.size(); } 
}