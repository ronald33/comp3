package comparative.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
 
public class App
{
    public static void main(String[] args) throws IOException 
    {
    	try
    	{
    		String folder = "gen";
    		long startTime = System.nanoTime();
    		
    		// REDUCE //
    		int n = 119; // Optimo
    		// 25 - optimo - una giga
    		int it = (int) Math.pow(2, n);
    		String reduce_filename = folder + "/" + "REDUCE_" + it;
    		
    		File reduce_file = new File(reduce_filename);
            FileWriter reduce_writer = new FileWriter(reduce_file, true);
    		
    		int it1 = (int) it / 3;
    		int it2 = (int) it / 3;
    		int it3 = (int) (it / 3) + 1;
    		
    		for (int i = 0; i < it1; i++)
    		{
        		float p1 = Helper.getRandom(0.4f, 0.5f);
        		float p2 = Helper.getRandom(0.2f, 0.3f);
        		float p3 = 1 - p1 - p2;
        		reduce_writer.write(p1 + " " + p2 + " " + p3 + "\n");
			}
    		
    		for (int i = 0; i < it2; i++)
    		{
        		float p1 = Helper.getRandom(0.2f, 0.3f);
        		float p2 = Helper.getRandom(0.5f, 0.6f);
        		float p3 = 1 - p1 - p2;
        		reduce_writer.write(p1 + " " + p2 + " " + p3 + "\n");
			}
    		
    		for (int i = 0; i < it3; i++)
    		{
        		float p1 = Helper.getRandom(0.2f, 0.3f);
        		float p2 = Helper.getRandom(0.2f, 0.3f);
        		float p3 = 1 - p1 - p2;
        		reduce_writer.write(p1 + " " + p2 + " " + p3 + "\n");
			}
    		
    		reduce_writer.close();
    		
    		// MAP //
    		String map_filename = folder + "/" + "MAP_" + it;
            File map_file = new File(map_filename);
            FileWriter map_writer = new FileWriter(map_file, true);
    		
    		for (int i = 0; i < it1; i++)
    		{
        		float p1 = Helper.getRandom(0.4f, 0.5f);
        		float p2 = 1 - p1;
    			map_writer.write(p1 + " " + p2 + "\n");
			}
    		
    		for (int i = 0; i < it2; i++)
    		{
    			float p1 = Helper.getRandom(0.2f, 0.3f);
        		float p2 = 1 - p1;
    			map_writer.write(p1 + " " + p2 + "\n");
			}
    		
    		for (int i = 0; i < it3; i++)
    		{
    			float p1 = Helper.getRandom(0.5f, 0.6f);
        		float p2 = 1 - p1;
    			map_writer.write(p1 + " " + p2 + "\n");
			}
    		
            map_writer.close();
            
            long stopTime = System.nanoTime();
			long elapsedTime = stopTime - startTime;
			System.out.println("EL tiempo total fue: " + elapsedTime);

    		System.out.println("Archivos generados correctamente =)");
    	}
    	catch(Exception e)
    	{
    		System.err.println(e.getMessage());
    	}
    }
}