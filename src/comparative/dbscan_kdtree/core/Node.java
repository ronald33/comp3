package comparative.dbscan_kdtree.core;

import java.util.List;

public class Node {
	
	public Node left;
	public Node right;
	public int position;
	public Point pointSplit;
	public Node parent;
	public List<Point> pointLeaf;
	
	public Node() {
	
	}
}
