package comparative.kmeans.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import comparative.kmeans.core.HostKMeans.TaskType;

public class ESAMR {

	HostKMeans hostKMeans;
//	public static final String path = "files/opt/hadoop-0.21.0-tests/runJobs/files/";
	public static final String path = "files/";
//	public static final Log LOG =
//			LogFactory.getLog(ESAMR.class);

	public ESAMR() {

	}
	
	public double[] calculateWeightsMapTasks(boolean isFirstJob, List<TaskItem> listTaskItem){

		double stageMapperWeigth, stageCombinerWeigth;
		int size = 0;
		
		if(listTaskItem != null){
			 size = listTaskItem.size();			
		}

		if(isFirstJob){

			if(size > 0){

				stageMapperWeigth = calculateMeanWeightsMapTasks(listTaskItem);
				stageCombinerWeigth = 1 - stageMapperWeigth;

			} else{

				stageMapperWeigth = 1.0f;
				stageCombinerWeigth = 0.0f;

			}
		} else{
			// exists running taks
			if(size > 0){

				stageMapperWeigth = calculateMeanWeightsMapTasks(listTaskItem);
				List<TaskItem>  listTaskItemTemp = readHistoricalInformationCluster(hostKMeans.JobID, TaskType.MAP);

				//random
				Random random = new Random();	
				int randIndex = (int) (random.nextDouble() *size);
				double stageMapperWeigthTemp = listTaskItemTemp.get(randIndex).getArrayWeights()[0];

				double beta = Math.abs(stageMapperWeigth - stageMapperWeigthTemp);  


				for (int j = 0; j < listTaskItemTemp.size(); j++) {

					double weights[]= listTaskItemTemp.get(j).getArrayWeights();

					if(Math.abs(weights[0] - stageMapperWeigth) < beta){

						stageMapperWeigthTemp = weights[0];
						beta = Math.abs(stageMapperWeigth - stageMapperWeigthTemp);

					}
				}
				
				stageMapperWeigth = stageMapperWeigthTemp;
				stageCombinerWeigth = stageMapperWeigth - 1;
				
			} else{
		
				List<TaskItem>  listTaskItemTemp = readHistoricalInformationCluster(hostKMeans.JobID, TaskType.MAP);

				stageMapperWeigth = calculateMeanWeightsMapTasks(listTaskItemTemp);
				stageCombinerWeigth = 1 - stageMapperWeigth;
			}
		}
		
		return new double[]{stageMapperWeigth, stageCombinerWeigth};
	}

	public double[] calculateWeightsReduceTasks(boolean isFirstJob, List<TaskItem> listTaskItem){

		double stageShuffleWeigth, stageSortWeigth, stageReduceWeigth;
		int size = 0;
		
		if(listTaskItem != null){
			 size = listTaskItem.size();			
		}

		if(isFirstJob){

			if(size > 0){

				stageShuffleWeigth = calculateMeanWeightsReduceTasks(listTaskItem)[0];
				stageSortWeigth = calculateMeanWeightsReduceTasks(listTaskItem)[1];
				stageReduceWeigth = 1 - (stageShuffleWeigth + stageSortWeigth);

			} else{
				
				stageShuffleWeigth = 0.33f;
				stageSortWeigth = 0.33f;
				stageReduceWeigth = 0.34f;

			}
		} else{
			// exists running taks
			if(size > 0){

				stageShuffleWeigth = calculateMeanWeightsReduceTasks(listTaskItem)[0];
				stageSortWeigth = calculateMeanWeightsReduceTasks(listTaskItem)[1];
				
				List<TaskItem>  listTaskItemTemp = readHistoricalInformationCluster(hostKMeans.JobID, TaskType.REDUCE);

				//random
				Random random = new Random();	
				int randIndex = (int) (random.nextDouble() *size);
				
				double stageShuffleWeigthTemp = listTaskItemTemp.get(randIndex).getArrayWeights()[0];
				double stageSortWeigthTemp = listTaskItemTemp.get(randIndex).getArrayWeights()[1];

				double beta = Math.abs(stageShuffleWeigth - stageShuffleWeigthTemp) + Math.abs(stageSortWeigth - stageSortWeigthTemp);  


				for (int j = 0; j < listTaskItemTemp.size(); j++) {

					double weights[]= listTaskItemTemp.get(j).getArrayWeights();

					if(Math.abs(weights[0] - stageShuffleWeigth) + Math.abs(weights[1] - stageSortWeigth)< beta){

						stageShuffleWeigthTemp = weights[0];
						stageSortWeigthTemp = weights[0];
						
						beta = Math.abs(stageShuffleWeigth - stageShuffleWeigthTemp) + Math.abs(stageSortWeigth - stageSortWeigthTemp);

					}
				}
				
				stageShuffleWeigth = stageShuffleWeigthTemp;
				stageSortWeigth =stageSortWeigthTemp;
				stageReduceWeigth = 1 - (stageShuffleWeigth + stageSortWeigth);				
				
			} else{
		
				List<TaskItem>  listTaskItemTemp = readHistoricalInformationCluster(hostKMeans.JobID, TaskType.REDUCE);
				
				stageShuffleWeigth = calculateMeanWeightsReduceTasks(listTaskItemTemp)[0];
				stageSortWeigth = calculateMeanWeightsReduceTasks(listTaskItemTemp)[1];
				stageReduceWeigth = 1 - (stageShuffleWeigth + stageSortWeigth);
			
			}
		}
		return new double[]{stageShuffleWeigth, stageSortWeigth, stageReduceWeigth};
	}
	/*
	public List<TaskItem> findSlowTasks(Map<TaskAttemptID, TaskTracker.TaskInProgress> runningTasks, double weight[]){
		
		
		for (int i = 0; i < runningTasks.size(); i++) {
			
			TaskInProgress tip = runningTasks.get(i);
			calculateProgressScore(tip, weight);
		} 
		
		return null;

	}

	public void calculateProgressScore(TaskInProgress tip, double weight[]){
		
		//tip.getStatus().getProgress();
		double progress = tip.getTask().getProgress().getProgress();

	}*/

	private double calculateMeanWeightsMapTasks(List<TaskItem> listTaskSItem){

		double stageMapperWeigthAverage;    
		double stageMapperWeigthSum = 0;    				
		TaskItem taskItem = null;
		int size = listTaskSItem.size();

		for (int i = 0; i < size; i++) {

			taskItem = listTaskSItem.get(i);
			stageMapperWeigthSum = stageMapperWeigthSum + taskItem.getArrayWeights()[0];

		}

		stageMapperWeigthAverage = stageMapperWeigthSum/size;

		return stageMapperWeigthAverage;
	}

	private double [] calculateMeanWeightsReduceTasks(List<TaskItem> listTaskSItem){

		double stageShuffleWeigthAverage, stageSortWeigthAverage;    
		double stageShuffleWeigthSum = 0, stageSortWeigthSum = 0;    				
		TaskItem taskItem = null;
		int size = listTaskSItem.size();

		for (int i = 0; i < size; i++) {

			taskItem = listTaskSItem.get(i);
			stageShuffleWeigthSum = stageShuffleWeigthSum + taskItem.getArrayWeights()[0];
			stageSortWeigthSum = stageSortWeigthSum + taskItem.getArrayWeights()[1];
			
		}

		stageShuffleWeigthAverage = stageShuffleWeigthSum/size;
		stageSortWeigthAverage = stageSortWeigthSum/size;

		return new double[]{stageShuffleWeigthAverage, stageSortWeigthAverage};
	}

	public List<TaskItem> readHistoricalInformation(String jobID, TaskType tasktype){

		BufferedReader inMap = null;
		BufferedReader inReduce = null;
		List<TaskItem>  listTaskItem = new ArrayList<TaskItem>();
		String line = null;

		try {

			if(tasktype == TaskType.MAP){

				File fileMap = new File(path + "mapWeights-" + jobID + ".txt");

				inMap = new BufferedReader(new FileReader(fileMap));	

				while ((line = inMap.readLine()) != null) {

					String value[] = line.split(" ");

					System.out.println(" >>>> read historical information: " + line);

					listTaskItem.add(new TaskItem(new double[]{Double.valueOf(value[3]), Double.valueOf(value[4])}, value[2]));					
					
				}

			} else{

				File fileReduce = new File(path + "reduceWeights-" + jobID + ".txt");
				inReduce = new BufferedReader(new FileReader(fileReduce));				

				while ((line = inReduce.readLine()) != null) {

					String value[] = line.split(" ");

					System.out.println(" >>>> read historical information: " + line);

					listTaskItem.add(new TaskItem(new double[]{Double.valueOf(value[3]), Double.valueOf(value[4]), Double.valueOf(value[5])}, 
							value[2]));

				}
			}	
		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {	
			try{
				if ( inMap != null ) inMap.close();
				if ( inReduce != null ) inReduce.close();	

			}catch ( IOException e ) {
				e.printStackTrace();
			}

		}

		return listTaskItem;
	}

	public List<TaskItem> readHistoricalInformationCluster(String jobID, TaskType tasktype){

		BufferedReader inMap = null;
		BufferedReader inReduce = null;
		List<TaskItem>  listTaskItem = new ArrayList<TaskItem>();
		String line = null;

		try {

			if(tasktype == TaskType.MAP){

				File fileMap = new File(path + "clusterMapWeights-" + jobID + ".txt");

				inMap = new BufferedReader(new FileReader(fileMap));	

				while ((line = inMap.readLine()) != null) {

					String value[] = line.split(" ");

					System.out.println(" >>>> read historical information: " + line);

					listTaskItem.add(new TaskItem(new double[]{Double.valueOf(value[0]), Double.valueOf(value[1])}, null));					

				}

			} else{

				File fileReduce = new File(path + "clusterReduceWeights-" + jobID + ".txt");
				inReduce = new BufferedReader(new FileReader(fileReduce));				

				while ((line = inReduce.readLine()) != null) {

					String value[] = line.split(" ");
					System.out.println(" >>>> read historical information: " + line);

					listTaskItem.add(new TaskItem(new double[]{Double.valueOf(value[0]), Double.valueOf(value[1]), Double.valueOf(value[2])}, null));

				}
			}	
		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {	
			try{
				if ( inMap != null ) inMap.close();
				if ( inReduce != null ) inReduce.close();	

			}catch ( IOException e ) {
				e.printStackTrace();
			}

		}

		return listTaskItem;
	}
	
	public  String[] getAllJobID(){

		File directory = new File(path);
		String JobID[] = new String[]{};
		int i = 0;		

		for (final File fileEntry : directory.listFiles()) {

			String value[] = fileEntry.getName().split("-");
			JobID[i] = value[1];
			i++;		  

		}

		return JobID;

	}

	public void writeRecordHistoricalInformation(String jobID, String taskID, String taskAttempID, 
			double arrayWeights[], TaskType tasktype){

		BufferedWriter outputMap = null;
		BufferedWriter outputReduce = null;

		// The structure is: <jobID> <taskID> <taskAttempID> <weight_1> ..  <weight_N> 

		try {

			if(tasktype == TaskType.MAP){

				File fileMap = new File(path+"mapWeights-"+jobID+".txt");

				outputMap = new BufferedWriter(new FileWriter(fileMap, true));	
				outputMap.write(jobID + " " + taskID + " " + taskAttempID + " "+ arrayWeights[0] + " " + arrayWeights[1]);
				outputMap.newLine();

			}else{

				File fileReduce = new File(path+"reduceWeights-"+jobID+".txt");

				outputReduce = new BufferedWriter(new FileWriter(fileReduce, true));
				outputReduce.write(jobID + " " + taskID + " " + taskAttempID + " "+ arrayWeights[0] + " " + arrayWeights[1] + " " + arrayWeights[2]);
				outputReduce.newLine();				

			}
		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {	
			try{
				if ( outputMap != null ) outputMap.close();
				if ( outputReduce != null ) outputReduce.close();	

			}catch ( IOException e ) {
				e.printStackTrace();
			}
		}

	}

	// Get and Set

	public HostKMeans getHostKMeans() {
		return hostKMeans;
	}

	public void setHostKMeans(HostKMeans hostKMeans) {
		this.hostKMeans = hostKMeans;
	}	
	
}