package comparative.dbscan_kdtree.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KdTree  {

	public Node root;
	public int dim;
	public List<Point> listPoint;
	//public final double distance_max = 9999999f;
	public double eps;
	public double minPts;
	
	public KdTree(int dim, List<Point> listPoint, double eps) {
		
		this.dim = dim;
 		this.listPoint = listPoint;
		this.eps = eps;
	}
	
	
	public void buildTree(){ 
		
		root = splitData(listPoint, 0);
	}
	
	public Node splitData(List<Point> listPoint, int level){
				
		Node node = new Node();
		int position = level % dim;
		int size = listPoint.size();
				
		if(size > 1){

			List<Point> lPointLeft = new ArrayList<Point>();
			List<Point> lPointRight = new ArrayList<Point>();
			double[] valueThresHold = calculateMean(listPoint, position);
			
			for (int i = 0; i < size; i++) {
				Point point = listPoint.get(i);
				double[] valuesTemp = point.getValues();
				
				if(valuesTemp[position] > valueThresHold[position]){
					lPointRight.add(point);
				} else{
					lPointLeft.add(point);
				}
			}
			
			if(lPointRight.size() ==  size || lPointLeft.size() ==  size){
				node.pointLeaf = new ArrayList<Point>();
				
				for (int i = 0; i < size; i++) {
					node.pointLeaf.add(listPoint.get(i));
				}
			}else{
				node.pointSplit = new Point(valueThresHold);
				node.position = position;
				level++;
				if(lPointLeft.size() > 0){
					node.left = splitData(lPointLeft, level);				
				}
				if(lPointRight.size() > 0){
					node.right = splitData(lPointRight, level);				
				}				
			}
		} else if(size == 1){
			node.pointLeaf = new ArrayList<Point>();
			node.pointLeaf.add(listPoint.get(0));
		} 
		
		return node;
		
	}
		
	public List<Point> searchNearestNeighborsPoints(Point query){

		List<Point> listNearest = new ArrayList<Point>();
		nearestNeighborsPoints2(query, root, listNearest, eps);
		
		return listNearest;
	}
	
	private double nearestNeighborsPoints(Point query, Node node, List<Point> listNearest, double distance){
		
		double[] values =  query.getValues();
		List<Point> leafs = node.pointLeaf;
		
		if(leafs  != null){

			Point leaf =leafs.get(0);
			double[] values_nearest = leaf.getValues();
			double sum = 0f;
			int size = values_nearest.length;
			
			for (int i = 0; i < size; i++) {
				sum = sum + Math.pow(values_nearest[i] - values[i], 2.0); 					
			}
			
			double distance_temp = Math.sqrt(sum);	

			if(distance_temp <= distance){
				int size_leafs = leafs.size();

				if(distance == eps){
					if(size_leafs > 1){
						for (int i = 0; i < size_leafs; i++) {
							listNearest.add(leafs.get(i));
						}
					}else{
						listNearest.add(leaf);						
					}	
				}else{
					distance = distance_temp;

					if(size_leafs > 1){
						for (int i = 0; i < size_leafs; i++) {
							listNearest.add(leafs.get(i));
						}
					}else{
						listNearest.add(leaf);
					}					
				}
			}	
			if(distance <= eps ){
				distance = eps;
			}	
		}else{
			
			double[] values_split = node.pointSplit.getValues();						
			int position = node.position;

			if(values[position] <= values_split[position]){
				
				if(values[position] - distance <= values_split[position]){
					if(node.left != null){
						distance = nearestNeighborsPoints(query, node.left, listNearest, distance);											
					}
				} 
				
				if(values[position] + distance > values_split[position]){
					if(node.right != null){
						distance = nearestNeighborsPoints(query, node.right, listNearest, distance);
					}
				}
								
			}else{
				
				if(values[position] + distance > values_split[position]){
					if(node.right != null){
						distance = nearestNeighborsPoints(query, node.right, listNearest, distance);											
					}
				} 
				
				if(values[position] - distance <= values_split[position]){
					if(node.left != null){
						distance = nearestNeighborsPoints(query, node.left, listNearest, distance);																
					}
				}
				
			}	
		}
		
		return distance;
	}

	private void nearestNeighborsPoints2(Point query, Node node, List<Point> listNearest, double eps){
		
		double[] values =  query.getValues();
		List<Point> leafs = node.pointLeaf;
		
		if(leafs  != null){

			Point leaf =leafs.get(0);
			double[] values_nearest = leaf.getValues();
			double sum = 0f;
			int size = values_nearest.length;
			
			for (int i = 0; i < size; i++) {
				sum = sum + Math.pow(values_nearest[i] - values[i], 2.0); 					
			}
			
			double distance_temp = Math.sqrt(sum);	

			if(distance_temp <= eps){
				int size_leafs = leafs.size();

				if(size_leafs > 1){
					for (int i = 0; i < size_leafs; i++) {
						listNearest.add(leafs.get(i));
					}
				}else{
					listNearest.add(leaf);						
				}	
			}	
		}else{
			
			double[] values_split = node.pointSplit.getValues();						
			int position = node.position;

			if(values[position] <= values_split[position]){
				
				if(values[position] - eps <= values_split[position]){
					if(node.left != null){
						 nearestNeighborsPoints2(query, node.left, listNearest, eps);											
					}
				} 
				
				if(values[position] + eps > values_split[position]){
					if(node.right != null){
						nearestNeighborsPoints2(query, node.right, listNearest, eps);
					}
				}
								
			}else{
				
				if(values[position] + eps > values_split[position]){
					if(node.right != null){
						nearestNeighborsPoints2(query, node.right, listNearest, eps);											
					}
				} 
				
				if(values[position] - eps <= values_split[position]){
					if(node.left != null){
						nearestNeighborsPoints2(query, node.left, listNearest, eps);																
					}
				}
				
			}	
		}
	}

	
	public double[] calculateMean(List<Point> point_temp, int position){
		
		int size = point_temp.size();
		double sum = 0f;
		
		for (int i = 0; i < size; i++) {
			sum = sum + point_temp.get(i).getValues()[position];
		}
		double result = sum/size; 
		double values[] = new double[dim]; 
		Arrays.fill(values, 0f);
		values[position] =  result;
		
		return values;
	}	
}