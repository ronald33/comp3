package comparative.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import comparative.dbscan_kdtree.core.Dbscan;
import comparative.dbscan_kdtree.core.Point;
import comparative.generator.Measure;
import comparative.kmeans.core.HostKMeans;
import comparative.kmeans.core.TaskCluster;
import comparative.kmeans.core.TaskItem;

public class Comparation {

	public enum AlgorithmType {KMEANS, DBSCAN_KDTREE};
	public BufferedReader inData = null;
	public BufferedWriter outData = null;
	public static final String path = "files/";
	private int exp;
			
	public void evaluateAlgorithm(AlgorithmType algorithm, int exp) throws Exception
	{
		this.exp = exp;
		
		switch (algorithm)
		{
			case KMEANS:
				Measure kmeans_m = new Measure();
				List<TaskCluster> listMapTaskCluster = new ArrayList<TaskCluster>();
				List<TaskCluster> listReduceTaskCluster = new ArrayList<TaskCluster>();

//				listMapTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
//				listMapTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
//
//				listReduceTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
//				listReduceTaskCluster.add(new TaskCluster(new ArrayList<TaskItem>(), null));
				
				List<TaskItem> listMapTaskItem = getTaskItems("MAP", this.exp);
				List<TaskItem> listReduceTaskItem = getTaskItems("REDUCE", this.exp);
				
				Measure m = new Measure();
				HostKMeans kmeans = new HostKMeans(listMapTaskCluster, listReduceTaskCluster, listMapTaskItem, listReduceTaskItem, "Job1");
				kmeans.run();
				m.showDuration("run de kmeans");
				kmeans_m.showDuration("Todo el Kmeans");
				
				break;
	
			case DBSCAN_KDTREE:
				Measure dbscan_m = new Measure();
//				double eps = 0.000005;
				double eps = Math.sqrt(0.1);
				int minPts = 2;
				
				Dbscan map_dbscan = new Dbscan(eps, minPts);
				List<Point> map_points = getPoints("MAP", this.exp);
				map_dbscan.setPoints(map_points);
				map_dbscan.initKdtree(2);
				
				Dbscan reduce_dbscan = new Dbscan(eps, minPts);
				List<Point> reduce_points = getPoints("REDUCE", this.exp);
				reduce_dbscan.setPoints(reduce_points);
				reduce_dbscan.initKdtree(3);
				
				Measure map_cluster_m = new Measure();
				map_dbscan.setClusters();
				map_cluster_m.showDuration("map_set_cluster");
				
				Measure reduce_cluster_m = new Measure();
				reduce_dbscan.setClusters();
				reduce_cluster_m.showDuration("reduce_set_cluster");
				
				System.out.println("El numero de clusters en map es: " + map_dbscan.getSizeClusters());
				System.out.println("El numero de clusters en reduce es: " + reduce_dbscan.getSizeClusters());
				
				dbscan_m.showDuration("Todo el dbscan");
				break;
		}
	}
	
	public List<Point> getPoints(String phase, int size) throws Exception
	{
		Measure m = new Measure();
		String folder = "gen";
		String filename = folder + "/" + phase + "_" + size;
		File file = new File(filename);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);
		String line;
		
		List<Point> points = new ArrayList<>();
		
		while((line = br.readLine()) != null)
		{
			String values[] = line.split(" ");
			Point p;
			if(phase == "MAP")
			{
				p = new Point(new double[]{Double.valueOf(values[0]), Double.valueOf(values[1])});
//				points.add();
			}
			else
			{
				p = new Point(new double[]{Double.valueOf(values[0]), Double.valueOf(values[1]), Double.valueOf(values[2])});
//				points.add(;
			}
			
			points.add(p);
		}
		br.close();
//		m.showDuration("asignar puntos");
		return points;
	}
	
	public List<TaskItem> getTaskItems(String phase, int size) throws Exception
	{
		Measure m = new Measure();
		String folder = "gen";
		String filename = folder + "/" + phase + "_" + size;
		File file = new File(filename);
		FileReader reader = new FileReader(file);
		BufferedReader br = new BufferedReader(reader);
		String line;
		
		List<TaskItem> taskItems = new ArrayList<TaskItem>();
		
		while((line = br.readLine()) != null)
		{
			String values[] = line.split(" ");
			TaskItem ti;
			
			if(phase == "MAP")
			{
				ti = new TaskItem(new double[]{Double.valueOf(values[0]), Double.valueOf(values[1])});
			}
			else
			{
				ti = new TaskItem(new double[]{Double.valueOf(values[0]), Double.valueOf(values[1]), Double.valueOf(values[2])});
			}
			
			taskItems.add(ti);
		}
		br.close();
//		m.showDuration("asignar puntos");
		return taskItems;
	}
}