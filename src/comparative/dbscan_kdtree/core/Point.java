package comparative.dbscan_kdtree.core;
import java.util.Arrays;


public class Point implements Comparable<Point>
{

	private double values[];
	
	private Boolean isVisited;
	private Boolean isNoise;
	
//	private int cluster;
	private Boolean hasCluster;
	
	public static int dimension;

	public Point() {
		
	}
	public Point(double [] values) {
		
		this.values = values;
		this.isVisited = false;
		this.isNoise = false;		
//		this.cluster = -1;
		this.hasCluster = false;
	}
		
	public double[] getValues() {
		return values;
	}
	public void setValues(double[] values) {
		this.values = values;
	}
	public Boolean isVisited() {
		return this.isVisited;
	}
	
	public void visited() {
		this.isVisited = true;
	}
	
	public void noise() {
		this.isNoise = true;
	}

//	public void setCluster(int cluster) {
//		this.cluster = cluster;
//	}
	
//	public Boolean unclustered() {
//		return this.cluster == -1;
//	}
	
	public void Clustered()
	{
		this.hasCluster = true;
	}
	
	public Boolean hasCluster()
	{
		return this.hasCluster;
	}
	
	public double distance (Point p)
	{
		double sum = 0;
		double [] values =  this.getValues();
		double [] values_point =  p.getValues();

		int size = values.length;
		
		for (int i = 0; i < size; i++)
		{
			double diff = values[i] - values_point[i];
			sum += Math.pow(diff, 2);
		}
		
		return Math.sqrt(sum);
    }

	@Override
	public String toString() {
		return "Point [values=" + Arrays.toString(values) + "]";
	}
	
	@Override
	public int compareTo(Point o) {
		
		int result = -1;
		
		if(this.values[dimension] >= o.values[dimension]){
			result = 1;
		}
		
		return result;
	}
	
}